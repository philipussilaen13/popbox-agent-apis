<!DOCTYPE html>
<html>
<head>
	<title>Data transaksi Tanggal 30 Januari 2019</title>
</head>
<body>
Hi Admin
<p>
Ada pesanan belanja stok dari {{ $transactions->userType }}. Mohon segera diproses ya. <br>
</p>
<table>
    <tr><td>No Transaksi</td><td>:</td><td><a href="{{ config('constant.report.url') }}/agent/transaction/detail/{{ $transactions->reference }}">{{ $transactions->reference }}</a></td></tr>
    <tr><td>Nama</td><td>:</td><td>{{ $transactions->name_user }}</td></tr>
    <tr><td>Nama Agent/Warung</td><td>:</td><td>{{ $transactions->locker_name }}</td></tr>
    <tr><td>Locker Id</td><td>:</td><td><a href="{{ config('constant.report.url') }}/agent/list/detail/{{ $transactions->locker_id }}">{{ $transactions->locker_id }}</a></td></tr>
    <tr><td>Alamat Agent/Warung</td><td>:</td><td>{{ $transactions->address }}</td></tr>
    <tr><td>No Telepon</td><td>:</td><td>{{ $transactions->phone }}</td></tr>
    <tr><td>Metode Pembayaran</td><td>:</td><td>{{ $transactions->paymentmethod }}</td></tr>
    <tr><td>Total Belanja Stok</td><td>:</td><td>Rp. {{ number_format($transactions->total_price,0,',','.') }}</td></tr>
</table>
<p>
    Berikut daftar barang yang dibeli:
</p>
<table class="tb_transaction">
    <thead>
        <tr>
            <th>No</th>
            <th>Barcode</th>
            <th>Product Name</th>
            <th>Qty</th>
            <th>Harga Satuan</th>
            <th>Harga Total</th>
        </tr>
    </thead>
    <tbody>
        @php $no=1; @endphp
        @foreach($transaction_items as $r)
            <tr>
                <td>{{ $no }}</td>
                <td>{{ $r->barcode }}</td>
                <td>{{ $r->productname }}</td>
                <td>{{ $r->qtyorder }}</td>
                <td>Rp. {{ number_format($r->totitem,0,',','.') }}</td>
                <td>Rp. {{ number_format($r->totprice,0,',','.') }}</td>
            </tr>
            @php $no++; @endphp
        @endforeach
    </tbody>
</table>
<p>
    <br />
    <br />
    Terima Kasih.
</p>
</body>
</html>
<style>
    body{
        border: 1px solid #d2d6de;
        padding: 10px 10px 10px 10px;
    }
    .head {
        font-size: 24px;
        color: red;
    }
    .tb_transaction {
        border-collapse: collapse;        
        width: 800px;
    }
    .tb_transaction > thead > tr > th {
        border: 1px solid #d2d6de;
        padding: 5px 5px 5px 5px;
    }
    .tb_transaction > tbody > tr > td {
        border: 1px solid #d2d6de;
        padding: 5px 5px 5px 5px;
    }
</style>
