<!DOCTYPE html>
<html>
<head>
	<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
	<title>PopBox Agent - Autr Ulang Kata Sandi</title>
	<meta charset="utf-8">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	{{-- Tell the browser to be responsive to screen width --}}
 	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  	{{-- Bootstrap 4.0.0.6 Alpha replaced with Daemonite Material Design --}}
  	<link rel="stylesheet" href="{{ asset('css/material.min.css') }}">
  	{{-- Font Awesome --}}
  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  	{{-- Ionicons --}}
  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  	{{-- Custom CSS --}}
  	<link rel="stylesheet" type="text/css" href="{{ asset('css/custom-auth.css') }}">
  	{{-- Floating Label --}}
  	<link rel="stylesheet" type="text/css" href="{{ asset('css/floating-labels.css') }}">

  	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  	<!--[if lt IE 9]>
  	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  	<![endif]-->
</head>
<body class="auth-login-body">
	<div class="auth-login-body-layer"></div>
	<div class="container">
		<div class="row justify-content-md-center align-items-center" style="height: 30%">
			<div class="col col-md-6 text-center">
				<h1 class="text-white" style="font-family: Impact">
					PopBox Agent
				</h1>
				<h3 class="text-white">
					Solusi Pintar untuk Pengiriman Anda
				</h3>
			</div>
		</div>
		<div class="row justify-content-md-center" style="height: 70%;">
			<div class="col col-md-4">
				<form action="{{ url('auth/reset') }}" method="post" class="div-transparent">
					{{ csrf_field() }}
					<input type="hidden" name="code" value="{{ $code }}">
					@if (count($errors) > 0)
					    <div class="alert alert-danger">
					        @foreach ($errors->all() as $error)
				                {{ $error }}
				            @endforeach
					    </div>
					@endif
					@if (\Session::has('error'))
						<div class="alert alert-danger text-white">
							{{ \Session::get('error') }}
						</div>
					@endif
					@if (\Session::has('success'))
						<div class="alert alert-success text-white">
							{{ \Session::get('success') }}
						</div>
					@endif
					<div class="text-center bg-inverse text-white div-header">
						<h3>Atur Ulang Kata Sandi</h3>
					</div>
					{{-- password --}}
			        <div class="form-group floating-label-form-group">
			            <label for="password">Kata Sandi</label>
			            <input type="password" class="form-control" placeholder="Kata Sandi" name="password" id="password" required="true" first="true" text="Kata Sandi">
			            <div class="form-control-feedback" id="password-feedback"></div>
			        </div>
			        {{-- repassword --}}
			        <div class="form-group floating-label-form-group">
			           	<label for="repassword">Konfirmasi Kata Sandi</label>
			            <input type="password" class="form-control" placeholder="Konfirmasi Kata Sandi" name="repassword" id="repassword" required="true" first="true" text="Kata Sandi">
			            <div class="form-control-feedback" id="repassword-feedback"></div>
			        </div>
			        <br> <br>
			        <div class="text-center">
			        	<button class="btn btn-danger">Kirim Atur Ulang Kata Sandi</button>
			        </div>
				</form>
			</div>
		</div>
	</div>

	{{-- jQuery --}}
	<script type="text/javascript" src="{{ asset('js/jquery-2.2.3.js') }}"></script>
	{{-- Tether --}}
	<script type="text/javascript" src="{{ asset('js/tether.min.js') }}"></script>
	{{-- Bootstrap --}}
	<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
	{{-- Deamonite Material JS --}}
    <script type="text/javascript" src="{{ asset('js/material.min.js') }}"></script>
	{{-- Float JS --}}
    <script type="text/javascript" src="{{ asset('js/floating-labels.js') }}"></script>

    <script type="text/javascript">
    	$(document).ready(function() {
    		/*Check value input*/
    		$('#userInput').add('#password').on('blur', function() {
    			var id = $(this).attr('id');
    			var check = checkInput(id);
    			if (check) {
    				$(this).parent().addClass('has-success');
    			}
    		});

    		/*Remove any error css on tipe in*/
    		$('input').on('keydown', function() {
    			$(this).parent().removeClass('has-warning');
    			var idElement = $(this).attr('id');
    			$('#'+idElement+"-feedback").html('');
    		});

    		/*Check Repassword*/
    		$('#repassword').on('blur', function(event) {
    			// check input
    			var checkIn = checkInput('repassword');
    			if (checkIn) {
    				checkRepassword();
    			}
    		});
    	});

    	/**
    	 * checking Input Value
    	 * @param  {[type]} idElement [description]
    	 * @return {[type]}           [description]
    	 */
    	function checkInput(idElement){
    		var element = $('#'+idElement);
    		var text = element.attr('text');
    		var value = element.val();
    		// check if input empty
    		if (value=='') {
    			element.parent().addClass('has-warning');
    			$('#'+idElement+"-feedback").html(text+' tidak boleh kosong');
    			return false;
    		}
    		// if email, check valid email
    		if (idElement=='email') {
    			var isValid = isEmail(value);
    			if (!isValid) {
    				element.parent().addClass('has-warning');
    				$('#'+idElement+"-feedback").html('Email Tidak Valid');
    				return false;
    			}
    		}
    		return true;
    	}

    	/**
    	 * check repassword
    	 * @return {boolean} [description]
    	 */
    	function checkRepassword(){
    		var password = $('#password').val();
    		var repassword = $('#repassword').val();

    		if (repassword !== password) {
    			$('#repassword-feedback').html('Kata Sandi Tidak Cocok');
    			$('#repassword-feedback').parent().addClass('has-warning');
    			return false;
    		}
    		$('#repassword-feedback').parent().addClass('has-success');
    		return true;
    	}
    </script>
</body>
</html>