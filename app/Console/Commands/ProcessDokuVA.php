<?php

namespace App\Console\Commands;

use App\Jobs\CreateTransactionNotification;
use App\Jobs\ProcessTransaction;
use App\Models\Locker;
use App\Models\Log;
use App\Models\Payment;
use App\Models\PaymentDokuVA;
use App\Models\PopboxDokuSettlement;
use App\Models\Transaction;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ProcessDokuVA extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'payment:dokuva';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process VA Payment from DOKU';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "Begin Process VA Payment from DOKU\n";
        $location = storage_path()."/logs/cron/";
        Log::logFile($location,'dokuVA',"Begin Proccess VA Payment");
        // get VA waiting and active
        $paymentDb = Payment::where('status','WAITING')
            ->has('dokuva')
            ->get();

        foreach ($paymentDb as $payment) {
            $transactionRef = $payment->transaction->reference;
            $amount = $payment->amount;
            echo "Transaction $transactionRef WAITING $amount\n";
            Log::logFile($location,'dokuVA',"Transaction $transactionRef WAITING $amount");

            // find data on doku settlement
            $dokuSettlement = PopboxDokuSettlement::where('transidmerchant',$transactionRef)->first();
            if (!$dokuSettlement) Log::logFile($location,'dokuVA',"$transactionRef Not Found on Doku Settlement");
            else {
                Log::logFile($location,'dokuVA',"$transactionRef exist on Doku Settlement");
                // check if transaction response_code = 0000
                echo "$transactionRef response code is $dokuSettlement->response_code\n";
                Log::logFile($location,'dokuVA',"$transactionRef response code is $dokuSettlement->response_code");

                if ($dokuSettlement->response_code != '0000'){
                    continue;
                }
                echo "Process \n";
                Log::logFile($location,'dokuVA',"Process");
                DB::beginTransaction();
                $transactionDb = Transaction::where('reference',$transactionRef)->first();
                $result = $this->success($transactionDb);
                if (!$result->isSuccess){
                    DB::rollback();
                    echo "Failed asd $result->errorMsg\n";
                    continue;
                }
                // change transaction and payment status to paid
                $transactionDb = Transaction::find($transactionDb->id);
                $transactionDb->status = 'PAID';
                $transactionDb->save();

                $paymentData = Payment::find($payment->id);
                $paymentData->status = 'PAID';
                $paymentData->save();

                $dokuVa = PaymentDokuVA::find($payment->dokuva->id);
                $dokuVa->status = 'PAID';
                $dokuVa->save();

                DB::commit();
            }
        }
        echo "Finish process VA\n";
        Log::logFile($location,'dokuVA','FInish Process');
    }

    private function success($transactionDb){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $location = storage_path()."/logs/cron/";

        if ($transactionDb->type=='topup'){
            echo "process topup\n";
            Log::logFile($location,'dokuVA',"Process TopUp");

            $result = Locker::processTopUp($transactionDb);
            if (!$result->isSuccess){
                $response->errorMsg = $result->errorMsg;
                return $response;
            }
            /*$username = $transactionDb->user->username;
            // create add 6500 to op up transfer fee
            $addTransactionDb = Transaction::createAdditionalTopUpTransaction($username,6500,null);
            if (!$addTransactionDb->isSuccess){
                $response->errorMsg = $addTransactionDb->errorMsg;
                return $response;
            }
            $addTransactionId = $addTransactionDb->transactionId;
            $addTransactionDb = Transaction::find($addTransactionId);
            $result = Locker::processTopUp($addTransactionDb);
            if (!$result->isSuccess){
                $response->errorMsg = $result->errorMsg;
                return $response;
            }
            $addTransactionDb->status = 'PAID';
            $addTransactionDb->save();*/

            $response->isSuccess = true;
        } else {
            echo "dispatch transaction\n";
            Log::logFile($location,'dokuVA',"Dispatch Transaction");
            dispatch(new ProcessTransaction($transactionDb));
            dispatch(new CreateTransactionNotification($transactionDb));
            $response->isSuccess = true;
        }
        return $response;
    }
}
