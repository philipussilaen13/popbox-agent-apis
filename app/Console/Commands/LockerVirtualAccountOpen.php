<?php

namespace App\Console\Commands;

use App\Http\Helpers\ApiPayment;
use App\Models\Locker;
use App\Models\LockerVAOpen;
use App\Models\Log;
use App\User;
use Illuminate\Console\Command;

class LockerVirtualAccountOpen extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'locker:vaopen';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Virtual Account Open Type on Locker Agent';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $location = storage_path()."/logs/cron/";
        echo "Get Locker List \n";
        Log::logFile($location,'lockerVAOpen',"Get Locker List");

        $availableOpen = ['bni_va','doku_bca_va'];
        // get all locker when not have va
        $lockerVADb = Locker::get();

        foreach ($lockerVADb as $item) {
            $lockerId = $item->id;
            $lockerName = $item->locker_name;
            echo "Locker ID $lockerId \n";
            // get locker va open
            foreach ($availableOpen as $openVa){
                echo "Query LockerId $lockerId with type $openVa \n";
                Log::logFile($location,'lockerVAOpen',"Query LockerId $lockerId with type $openVa");
                $lockerVaOpenDb = LockerVAOpen::where('lockers_id',$lockerId)->where('va_type',$openVa)->first();
                if ($lockerVaOpenDb) {
                    echo "Already Have $openVa \n";
                    Log::logFile($location,'lockerVAOpen',"Already Have $openVa");
                    continue;
                }
                Log::logFile($location,'lockerVAOpen',"Locker ID $lockerId");
                // get user phone
                $userDb = User::where('locker_id',$lockerId)
                    ->where('status','<>','0')
                    ->first();
                if (!$userDb) {
                    echo "Doesnt Have User  or Not Actice\n";
                    Log::logFile($location,'lockerVAOpen',"Doesnt Have User or Not Actice");
                    continue;
                }
                // get phone
                $phone = $userDb->phone;
                /*$testingPhone = ['08563052300','085710157057'];
                if (!in_array($phone,$testingPhone)) {
                    echo "Not Dev Number \n";
                    Log::logFile($location,'lockerVAOpen',"Not Dev Number");
                    continue;
                }*/

                $vaNumber = substr($phone,-10);
                echo "$lockerId -> $phone -> VA Number = $vaNumber \n";
                Log::logFile($location,'lockerVAOpen',"$lockerId -> $phone -> VA Number = $vaNumber");

                $expiredYears = 50;

                $method = null;
                if ($openVa == 'bni_va') $method = 'BNI-VA';
                elseif ($openVa == 'doku_bca_va') $method = 'DOKU-BCA';

                $param = [];
                $param['method_code'] = $method;
                $param['amount'] = '0';
                $param['billing_type'] = 'open';
                $param['transaction_id'] = "$lockerId-$method";
                $param['customer_name'] = "PT POPBOX ASIA - $lockerName";
                $param['description'] = 'Top Up Open '.$lockerName;
                $param['virtual_account'] = $vaNumber;
                $param['datetime_expired'] = date('Y-m-d H:i:s',strtotime("+$expiredYears years"));

                // post to API
                $apiV2 = new ApiPayment();
                $result =  $apiV2->createPayment($param);

                echo "Push to API Payment \n";
                Log::logFile($location,'lockerVAOpen',"Push to API Payment");

                if (empty($result)){
                    $message = "Failed to Push Doku API";
                    echo "$message";
                    Log::logFile($location,'lockerVAOpen',"$message");
                    continue;
                }
                if ($result->response->code!=200){
                    $message = $result->response->message;
                    echo "$message";
                    Log::logFile($location,'lockerVAOpen',"$message");
                    continue;
                }
                $data = $result->data[0];

                $vaNumber = $data->virtual_account_number;
                $expiredDate = $data->datetime_expired;
                $paymentId = $data->payment_id;

                $lockerVAData = new LockerVAOpen();
                $lockerVAData->lockers_id = $lockerId;
                $lockerVAData->va_type = $openVa;
                $lockerVAData->va_number = $vaNumber;
                $lockerVAData->payment_id = $paymentId;
                $lockerVAData->expired_date = $expiredDate;
                $lockerVAData->save();

                $message = 'Save to Locker VA Open';
                echo "$message";
                Log::logFile($location,'lockerVAOpen',"$message");
            }
        }
    }
}
