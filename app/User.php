<?php

namespace App;

use App\Http\Helpers\Helper;
use App\Models\Cart;
use App\Models\Locker;
use App\Models\VirtualLocker;
use App\Models\PendingTransaction;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = [
        'deleted_at'
    ];

    /**
     * insert agent user to DB and to Virtual Locker
     * @param $input
     * @return \stdClass
     */
    public static function insertAgentUser($input){
        // generate general response
        $response =  new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->code = null;

        // check on KTP Number
        $idNumber = $input['id_number'];
        $check = self::where('id_number',$idNumber)->first();
        if ($check){
            $response->errorMsg = 'Nomor identitas sudah terdaftar di sistem';//'ID Number already Exist';
            return $response;
        }

        // get all parameter
        $name = $input['name'];
        $phone = $input['phone'];
        $email = empty($input['email']) ? null : $input['email'];
        $username = $input['phone'];
        $password = Hash::make($input['password']);

        $status = 0;
        $gcm_token = empty($input['gcm_token']) ? null : $input['gcm_token'];
        $idNumber = $input['id_number'];
        $idType = $input['id_type'];

        //saving image
        $idPicture = $input['id_picture'];
        // generate filename
        $filename = $username.'-'.date('Ymd').'.jpg';
        $path = public_path()."/agent/id/$filename";
        $img = base64_decode($idPicture);
        $saving = file_put_contents($path,$img);
        if (!$saving){
            $response->errorMsg = "Failed to Save Id";
            return $response;
        }

        // generate code verification
        $randomString = Helper::generateRandomString(20);
        $isExist = true;
        $codeVerification = null;
        while ($isExist){
            $codeVerification = date('Ymd').$randomString;
            $check = self::where('verification',$codeVerification)->first();
            if (!$check) $isExist = false;
        }

        // insert to DB
        $agentDb = new self();
        $agentDb->name = $name;
        $agentDb->phone = $phone;
        $agentDb->email = $email;
        $agentDb->username = $username;
        $agentDb->password = $password;
        $agentDb->picture = $filename;
        $agentDb->status = $status;
        $agentDb->gcm_token = $gcm_token;
        $agentDb->id_number = $idNumber;
        $agentDb->id_type = $idType;
        $agentDb->id_picture = $filename;
        $agentDb->verification = $codeVerification;
        $agentDb->save();

        // get agent id
        $id = $agentDb->id;

        $response->isSuccess = true;
        $response->id = $id;
        $response->code = $codeVerification;

        return $response;
    }

    /**
     * update agent user to DB and to Virtual Locker
     * @param $input
     * @return \stdClass
     */
    public static function updateAgentUser($input){
        // generate general response
        $response =  new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // get all parameter
        $name = empty($input['name'])? null : $input['name'];
        $phone = empty($input['phone'])? null : $input['phone'];
        $email = empty($input['email'])? null : $input['email'];

        $password = empty($input['password'])? null : $input['password'];
        $locker_id = empty($input['locker_id']) ? null : $input['locker_id'];

        $username = $input['username'];

        // saving image
        $picture =empty($input['picture']) ? null : $input['picture'];
        if (!empty($picture)){
            // generate filename
            $filename = $username.'-'.date('Ymd').'.jpg';
            $path = public_path()."/agent/picture/$filename";
            $img = base64_decode($picture);
            $saving = file_put_contents($path,$img);
            if (!$saving){
                $response->errorMsg = "Failed to Save Picture";
                return $response;
            }
        }

        $idNumber =empty($input['id_number']) ? null : $input['id_number'];
        $idType =empty($input['id_type']) ? null : $input['id_type'];
        //saving image
        $username = $input['username'];

        DB::beginTransaction();
        // update to db
        $agentDb = self::where('username',$username)->first();
        $agentDb = self::find($agentDb->id);
        // checking phone
        if ($phone){
            $phoneData = self::where('phone',$phone)->first();
            if ($phoneData){
                if ($phoneData->id != $agentDb->id){
                    $response->errorMsg = 'Phone Number Already Exist';
                    return $response;
                }
            }
        }
        // checking email
        if ($email){
            $emailData = self::where('email',$email)->first();
            if ($emailData){
                if ($emailData->id != $agentDb->id){
                    $response->errorMsg = 'Email Already Exist';
                    return $response;
                }
            }
        }

        if ($name) $agentDb->name = $name;
        if ($phone) $agentDb->phone = $phone;
        if ($email) $agentDb->email = $email;
        if ($password) $agentDb->password = Hash::make($password);
        if ($picture) $agentDb->picture = $picture;
        if ($locker_id) $agentDb->locker_id = $locker_id;
        $agentDb->status = 0;

        $agentDb->save();

        $id = $agentDb->id;

        DB::commit();
        $response->id = $id;
        $response->isSuccess = true;
        return $response;
    }

    /**
     * activate agent
     * @param $input
     * @return \stdClass
     */
    public static function ActivateAgentUser($input){
        // generate general response
        $response =  new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $code = $input['code'];
        // find code on table popbox agent user
        $agentDb = self::where('verification',$code)->first();
        if (!$agentDb){
            $response->errorMsg = 'Invalid Verification Code';
            return $response;
        }
        $id = $agentDb->id;
        $agentDb = self::find($id);
        // update status to 1
        $agentDb->status = '1';
        $agentDb->save();

        $response->isSuccess = true;
        return $response;
    }

    private static function postApi($url,$params){
        $url = env('VL_URL','http://locker.dev/api/').$url;
        $params['token'] = env('VL_TOKEN','79XM983RH8TK37KTR84NNUCYK8DTLTRDXE5Z77UP');

        $headers = array (
            'Content-Type: application/json'
        );
        $result = null;
        $curl = curl_init ();
        $params = json_encode($params);

        $date = date('Y.m.d');
        $time = date('H:i:s');
        $msg = "$time Request : $url : $params\n";
        $f = fopen(storage_path().'/logs/api/'.$date.'.log','a');
        fwrite($f,$msg);
        fclose($f);

        curl_setopt ( $curl, CURLOPT_URL, $url );
        curl_setopt ( $curl, CURLINFO_HEADER_OUT, true);
        curl_setopt ( $curl, CURLOPT_POST, 1 );
        curl_setopt ( $curl, CURLOPT_POSTFIELDS, $params );
        curl_setopt ( $curl, CURLOPT_COOKIEJAR, "" );
        curl_setopt ( $curl, CURLOPT_FOLLOWLOCATION, true );
        curl_setopt ( $curl, CURLOPT_ENCODING, "" );
        curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, true );
        curl_setopt ( $curl, CURLOPT_AUTOREFERER, true );
        curl_setopt ( $curl, CURLOPT_SSL_VERIFYPEER, false ); // required for https urls
        curl_setopt ( $curl, CURLOPT_CONNECTTIMEOUT, 5 );
        curl_setopt ( $curl, CURLOPT_TIMEOUT, 5 );
        curl_setopt ( $curl, CURLOPT_MAXREDIRS, 10 );
        curl_setopt ( $curl, CURLOPT_HTTPHEADER, $headers );

        $content = curl_exec ( $curl );
        $response = curl_getinfo ( $curl );

        curl_close ( $curl );

        DB::table('companies_response')
            ->insert([
                'api_url' => $url,
                'api_send_data' => $params,
                'api_response'  => $content,
                'response_date'     => date("Y-m-d H:i:s")
            ]);

        $time = date('H:i:s');
        $msg = "$time Response : $content\n";
        $f = fopen(storage_path().'/logs/api/'.$date.'.log','a');
        fwrite($f,$msg);
        fclose($f);

        return $content;
    }

    /* Relationship */
    public function carts(){
        return $this->hasMany(Cart::class,'users_id','id');
    }

    public function locker(){
        return $this->belongsTo(Locker::class,'locker_id','id');
    }

    public function virtualLocker(){
        return $this->belongsTo(VirtualLocker::class,'locker_id','locker_id');
    }

    public function pendingTransactions(){
        return $this->hasMany(PendingTransaction::class,'user_id','id');
    }
}
