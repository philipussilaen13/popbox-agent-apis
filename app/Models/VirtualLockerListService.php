<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VirtualLockerListService extends Model
{
    // set table and connection
    protected $connection = 'popbox_virtual';
    protected $table = 'list_locker_services';
}
