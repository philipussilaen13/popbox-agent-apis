<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VirtualLockerCity extends Model
{
    protected $connection = 'popbox_virtual';
    protected $table = 'cities';

    public function province(){
        return $this->belongsTo(VirtualLockerProvince::class,'provinces_id','id');
    }

    public function region() {
        return $this->belongsTo(VirtualRegion::class,'regions_id','id');
    }
}
