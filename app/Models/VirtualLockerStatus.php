<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VirtualLockerStatus extends Model
{
    // define connection
    protected $connection = 'popbox_virtual';
    protected $table = 'locker_status';

    /**
     * change status locker
     * @param $lockerId
     * @param string $status
     * @param null $userId
     * @param null $latitude
     * @param null $longitude
     * @return \stdClass
     */
    public static function changeStatus($lockerId, $status='OFFLINE',$userId = null,$latitude = null, $longitude = null){
        $response = new \stdClass();
        $response->isSuccess = false;

        // check array of status
        $statusList = ['ONLINE','WARNING','OFFLINE','OUTRANGE'];
        if (!in_array($status,$statusList)){
            return $response;
        }

        // find locker Id
        $virtualLockerDb = VirtualLocker::where('locker_id',$lockerId)->first();
        if (!$virtualLockerDb){
            return $response;
        }
        $statusNumber = 2;
        if ($status == 'ONLINE') $statusNumber =1;
        // update locker
        $virtualLockerDb = VirtualLocker::find($virtualLockerDb->id);
        $virtualLockerDb->status = $statusNumber;
        $virtualLockerDb->last_ping = date('Y-m-d H:i:s');
        $virtualLockerDb->last_location = "$latitude,$longitude";
        $virtualLockerDb->save();

        // create status history
        $statusDb = VirtualLockerStatus::where('lockers_id',$virtualLockerDb->id)->first();
        if (!$statusDb){
            // if not exist insert into db
            $statusDb = new VirtualLockerStatus();
            $statusDb->lockers_id = $virtualLockerDb->id;
            $statusDb->status = $status;
            $statusDb->save();
            // insert new history
            VirtualLockerStatusDetail::insertStatusDetail($statusDb->id,$status);
        }

        // if status not same
        if ($statusDb->status != $status){
            // update status
            $statusDb = VirtualLockerStatus::find($statusDb->id);
            $statusDb->status = $status;
            $statusDb->save();
            VirtualLockerStatusDetail::insertStatusDetail($statusDb->id,$status);
        }

        if (!empty($userId) && !empty($latitude) && !empty($longitude)){
            // insert into user location
            $userLocationDb = new UserLocation();
            $userLocationDb->users_id = $userId;
            $userLocationDb->latitude = $latitude;
            $userLocationDb->longitude = $longitude;
            $userLocationDb->save();
        }

        $response->isSuccess = true;
        return $response;
    }
}
