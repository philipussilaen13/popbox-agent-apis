<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReferralCampaign extends Model
{
    protected $table = 'referral_campaigns';

    /**
     * Get Active Campaign
     * @return \stdClass
     */
    public static function getActiveReferralCampaign(){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->campaignId = null;

        $nowDateTime = date('Y-m-d H:i:s');
        $referralCampaignDb = ReferralCampaign::where('status','=',1)
            ->where('start_date','<=',$nowDateTime)
            ->where('end_date','>=',$nowDateTime)
            ->whereNull('code')
            ->orderBy('created_at','desc')
            ->first();
        if (!$referralCampaignDb){
            $response->errorMsg = 'No Active Referral Campaign';
            return $response;
        }
        $response->campaignId = $referralCampaignDb->id;
        $response->isSuccess = true;
        return $response;
    }

    /**
     * Check and Get Active Campaign based on referral code
     * @param $referralCode
     * @return \stdClass
     */
    public static function checkingActiveCampaign($referralCode){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->campaignId = null;

        // check code from campaign first
        $referralCampaignDb = self::where('code',$referralCode)->first();
        if (!$referralCampaignDb){
            // if not available on referral campaign db check on lockers db
            $lockerDb = Locker::where('referral_code',$referralCode)->first();
            if (!$lockerDb){
                // referral code not found / invalid
                $message = 'Referral Code Not Found / Invalid.';
                $response->errorMsg = $message;
                return $response;
            }
            // get referral active campaign
            $check = self::getActiveReferralCampaign();
            if (!$check->isSuccess){
                $message = $check->errorMsg;
                $response->errorMsg = $message;
                return $response;
            }
            $referralCampaignDb = self::find($check->campaignId);
        }

        $response->campaignId = $referralCampaignDb->id;
        $response->isSuccess = true;
        return $response;
    }

    /**
     * Check Usage Referral Code
     * @param $referralCode
     * @return \stdClass
     */
    public static function checkUsage($referralCode){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->limitUsage = 0;
        $response->usage = 0;

        // check from referral campaign first
        $referralCampaignDb = ReferralCampaign::where('code',$referralCode)->first();
        if (!$referralCampaignDb){
            // check on locker table
            $lockerDb = Locker::where('referral_code',$referralCode)->first();
            if (!$lockerDb){
                $response->errorMsg = 'Invalid Referral Code';
                return $response;
            }
        }

        // check get active campaign first
        $activeCampaign = self::checkingActiveCampaign($referralCode);
        if (!$activeCampaign->isSuccess){
            $response->errorMsg = $activeCampaign->errorMsg;
            return $response;
        }
        $campaignId = $activeCampaign->campaignId;
        $referralCampaignDb = ReferralCampaign::find($campaignId);

        // get usage
        $referralTransactionDb = ReferralTransaction::with('referral')
            ->where('code',$referralCode)
            ->where(function ($query){
                $query->where('status','USED')
                    ->orWhere(function ($query){
                        $nowDateTime = date('Y-m-d H:i:s');
                        $query->where('status','PENDING')
                            ->where('expired_date','>',$nowDateTime);
                    });
            })->get();

        $usage = count($referralTransactionDb);

        $response->limitUsage = empty($referralCampaignDb->limit_usage) ? 0 : $referralCampaignDb->limit_usage;
        $response->usage = $usage;
        $response->isSuccess = true;

        return $response;
    }


    /*===================================Relationship===================================*/

    /**
     * One referral campaign has many referral transaction
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactions(){
        return $this->hasMany(ReferralTransaction::class);
    }
}
