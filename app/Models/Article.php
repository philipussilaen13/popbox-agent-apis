<?php

namespace App\Models;

use function foo\func;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    // set table
    protected $table = 'articles';

    public static function listArticles($input){
        $type = empty($input['type']) ?  null : $input['type'];
        $value = empty($input['value']) ? null : $input['value'];
        $title = empty($input['title']) ? null : $input['title'];
        $status = empty($input['status']) ? 1 : $input['status'];
        $articleId = empty($input['article_id']) ? null : $input['article_id'];

        // query active
        $nowDate = date('Y-m-d H:i:s');
        $articleDb = self::where('status',$status)
            ->when($type,function($query)use($type){
                return $query->where('type',$type);
            })->when($value,function ($query) use($value){
                return $query->where('value',$value);
            })->when($title,function ($query) use ($title){
                return $query->where('title','LIKE',"%$title%");
            })->when($articleId,function ($query) use ($articleId){
                return $query->where('id',$articleId);
            })->where('start_date','<=',$nowDate)
            ->where('end_date','>=',$nowDate)
            ->get();

        return $articleDb;
    }
}
