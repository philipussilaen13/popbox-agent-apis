<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class BalanceRecord extends Model
{
    // set table
    protected $table = 'balance_records';

    /**
     * insert new record
     * @param $lockerId
     * @param $transactionId
     * @param int $credit
     * @param int $debet
     * @param $balance
     */
    public static function insertNewRecord($lockerId,$transactionId,$credit=0,$debit=0,$balance){
        $data = new self();
        $data->lockers_id = $lockerId;
        $data->transactions_id = $transactionId;
        $data->credit = $credit;
        $data->debit = $debit;
        $data->balance = $balance;
        $data->date = date('Y-m-d');
        $data->save();

        return;
    }

    /**
     * list record
     * @param $username
     * @return \stdClass
     */
    public static function listRecord($username){
        // standard response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // get user locker
        $userDB = User::where('username',$username)->first();
        if (!$userDB){
            $response->errorMsg = 'Invalid User';
            return $response;
        }
        $lockerId = $userDB->locker_id;
        $balanceData = [];
        // get balance record activity
        $balanceDb = self::where('lockers_id',$lockerId)
            ->orderBy('id','desc')
            ->get();

        foreach ($balanceDb as $item){
            $tmp = new \stdClass();
            $tmp->credit = $item->credit;
            $tmp->debit = $item->debit;
            $tmp->balance = $item->balance;
            $tmp->transaction_ref = $item->transaction->reference;
            $tmp->transaction_date = $item->date;
            $tmp->transaction_datetime = date('Y-m-d H:i:s',strtotime($item->created_at));

            $balanceData[] = $tmp;
        }
        if (empty($balanceData)){
            $response->errorMsg = 'Empty Balance Record';
            return $response;
        }
        $response->isSuccess = true;
        $response->data = $balanceData;
        return $response;
    }

    /**
     * credit / top up deposit
     * @param $lockerId
     * @param $amount
     * @param $transactionId
     * @return \stdClass
     */
    public static function creditDeposit($lockerId, $amount, $transactionId)
    {
        // generate standard response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // get lockerId Deposit
        $lockerDb = Locker::find($lockerId);
        if (!$lockerDb) {
            $response->errorMsg = 'Invalid Locker';
            return $response;
        }
        $currentBalance = $lockerDb->balance;
        // credit deposit
        $newBalance = $currentBalance + $amount;

        // update to locker balance
        $lockerDb->balance = $newBalance;
        $lockerDb->save();

        // insert into balance record
        $credit = $amount;
        $debit = 0;
        $balanceRecordDb = self::insertNewRecord($lockerId, $transactionId, $credit, $debit, $newBalance);

        $response->isSuccess = true;
        return $response;
    }


    /*Relationship*/
    public function transaction(){
        return $this->hasOne(Transaction::class,'id','transactions_id');
    }
}
