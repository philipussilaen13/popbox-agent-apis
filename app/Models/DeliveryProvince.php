<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryProvince extends Model
{
    protected $table = 'delivery_provinces';

    /*relationship*/
    public function cities(){
        return $this->hasMany(DeliveryCity::class);
    }
}
