<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryOrder extends Model
{
    protected $table = 'delivery_orders';

    /**
     * Create Order
     * @param $lockerId
     * @param $pickupDate
     * @param $pickupTime
     * @param $pickupLocation
     * @param $awbNumber
     * @param $destinationDistrictCode
     * @param $actualWeight
     * @param $param
     * @param $calculateResult
     * @return \stdClass
     */
    public function createOrder($lockerId,$pickupDate,$pickupTime,$pickupLocation,$awbNumber,$destinationDistrictCode,$actualWeight,$param,$calculateResult){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->orderId = null;

        $dataDb = new self();
        $dataDb->locker_id = $lockerId;
        $dataDb->pickup_date = date('Y-m-d',strtotime($pickupDate));
        $dataDb->pickup_time = $pickupTime;
        $dataDb->pickup_location = $pickupLocation;
        $dataDb->awb_number = $awbNumber;
        $dataDb->sender_phone = $param['sender_telp'];
        $dataDb->sender_name = $param['sender_name'];
        $dataDb->sender_email = $param['sender_email'];
        $dataDb->recipient_name = $param['recipient_name'];
        $dataDb->recipient_phone = $param['recipient_telp'];
        $dataDb->recipient_email = $param['recipient_email'];
        $dataDb->recipient_address = $param['recipient_address'];
        $dataDb->destination_district_code = $destinationDistrictCode;
        $dataDb->reseller = $param['reseller'];
        $dataDb->service_type = $param['service_type'];
        $dataDb->weight = $calculateResult->weight;
        $dataDb->actual_weight = $actualWeight;
        $dataDb->status = 'CREATE';
        $dataDb->price_per_kg = $calculateResult->price_per_kg;
        $dataDb->parcel_price = $param['parcel_price'];
        $dataDb->insurance = $calculateResult->insurance_price;
        $dataDb->total_price = $calculateResult->total_price;
        $dataDb->pickup_number = $calculateResult->pickup_number;
        $dataDb->save();

        $deliveryId = $dataDb->id;

        // create history
        $deliveryHistory = new DeliveryOrderHistory();
        $createNew = $deliveryHistory->newHistory($deliveryId,'CREATE');

        $response->isSuccess = true;
        $response->orderId = $deliveryId;
        return $response;
    }

    /**
     * Update Status Based on AWB number
     * @param $awbNumber
     * @param $status
     * @param null $remarks
     * @return \stdClass
     */
    public function updateStatus($awbNumber,$status,$remarks=null){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // get delivery order
        $dataDb = self::where('awb_number',$awbNumber)->first();
        if (!$dataDb){
            $response->errorMsg = 'Invalid Order Number';
            return $response;
        }

        // update
        $dataDb = self::find($dataDb->id);
        $dataDb->status = $status;
        $dataDb->save();

        // create history
        $deliveryHistory = new DeliveryOrderHistory();
        $new = $deliveryHistory->newHistory($dataDb->id,$status,$remarks);

        $response->isSuccess = true;
        return $response;
    }

    /**
     * Get Orders
     * @param array $param
     * @return mixed
     */
    public function getOrders($param=[]){
        $lockerId = empty($param['locker_id'])? null : $param['locker_id'];
        $pickupDate = empty($param['pickup_date'])? null : $param['pickup_date'];
        $awbNumber = empty($param['awb_number'])? null : $param['awb_number'];
        $senderPhone = empty($param['sender_phone'])? null : $param['sender_phone'];
        $senderName = empty($param['sender_name'])? null : $param['sender_name'];
        $recipientPhone = empty($param['recipient_phone'])? null : $param['recipient_phone'];
        $recipientName = empty($param['recipient_name'])? null : $param['recipient_name'];
        $serviceType = empty($param['service_type'])? null : $param['service_type'];
        $status = empty($param['status'])? null : $param['status'];
        $beginDate = empty($param['begin_date'])? date('Y-m-1 00:00:00') : $param['begin_date']." 00:00:00";
        $endDate = empty($param['end_date'])? date('Y-m-t 23:59:59') : $param['end_date']." 23:59:59";

        $dataDb = self::when($lockerId,function ($query) use($lockerId){
            return $query->where('locker_id',$lockerId);
        })->when($pickupDate,function ($query) use($pickupDate){
            return $query->where('pickup_date',$pickupDate);
        })->when($awbNumber,function ($query) use($awbNumber){
            return $query->where('awb_number',$awbNumber);
        })->when($status, function ($query) use($status){
            return $query->where('status',$status);
        })->whereBetween('created_at',[$beginDate,$endDate])
            ->orderBy('created_at','desc')
            ->get();

        $orderList = [];

        foreach ($dataDb as $item) {
            $response = new \stdClass();
            $response->locker_id = $lockerId;
            $response->awb_number = $item->awb_number;
            $response->sender_phone = $item->sender_phone;
            $response->sender_name = $item->sender_name;
            $response->sender_email = $item->sender_email;
            $response->recipient_phone = $item->recipient_phone;
            $response->recipient_name = $item->recipient_name;
            $response->recipient_email = $item->recipient_email;
            $response->recipient_address = $item->recipient_address;
            $response->pickup_location = $item->pickup_location;
            $response->destination_province = $item->district->city->province->province_name;
            $response->destination_city = $item->district->city->city_name;
            $response->destination_district = $item->district->district_name;
            $response->service_type = $item->service_type;
            $response->weight = $item->weight;
            $response->price_per_kg = $item->price_per_kg;
            $response->insurance_price = (int)$item->insurance;
            $response->total_price = $item->total_price;
            $response->created_at = date('Y-m-d H:i:s',strtotime($item->created_at));
            $response->updated_at = date('Y-m-d H:i:s',strtotime($item->updated_at));

            $histories = [];
            $orderHistory = $item->histories;
            foreach ($orderHistory as $history) {
                $tmp = new \stdClass();
                $tmp->status = $history->status;
                $tmp->remarks = $history->remarks;
                $tmp->date = date('Y-m-d H:i:s',strtotime($history->created_at));
                $histories[] = $tmp;
            }
            $response->histories = $histories;

            $orderList[] = $response;
        }

        return $orderList;
    }

    /*Relationship*/
    public function histories(){
        return $this->hasMany(DeliveryOrderHistory::class);
    }

    public function district(){
        return $this->belongsTo(DeliveryDistrict::class,'destination_district_code','code');
    }
}
