<?php

namespace App\Models;

use App\Http\Helpers\Helper;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class PasswordReset extends Model
{
    // set table
    protected $table = 'password_resets';

    /**
     * insert new request for reset password link
     * @param User $userDb
     * @return \stdClass
     */
    public static function insertNew(User $userDb){
        // create default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->code = null;

        // input to DB
        DB::beginTransaction();

        // disable active reset link
        self::disableAllActive($userDb->email);

        $dataDb = new self();
        $dataDb->users_id = $userDb->id;
        $dataDb->email = $userDb->email;
        $dataDb->phone = $userDb->phone;

        // create code
        $randomString = Helper::generateRandomString(10);
        $code = $randomString.":".strtoupper($userDb->email);
        $randomString = Helper::generateRandomString(10);
        $code .= ":".$randomString;
        $dataDb->code = $code;

        $dataDb->status = 1;
        $dataDb->valid_time = Carbon::now()->addDay(3);
        $dataDb->save();

        DB::commit();
        $response->isSuccess = true;
        $response->code = $code;
        return $response;
    }

    /**
     * reset password
     * @param $code
     * @param $password
     * @return \stdClass
     */
    public static function resetPassword($code,$password){
        // default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // get reset Password db
        $resetDb = self::where('code',$code)->where('status','1')->first();
        if (!$resetDb){
            $response->errorMsg = 'Invalid Code Reset Password';
            return $response;
        }
        if (!empty($resetDb->reseted_at)){
            $response->errorMsg = 'Code have been used';
            return $response;
        }
        if (strtotime($resetDb->valid_time) < strtotime(date('Y-m-d'))){
            $response->errorMsg = 'Expired Code';
            return $response;
        }

        // get user Db
        $userDb = User::find($resetDb->users_id);

        // update to DB
        DB::beginTransaction();
        // update reset password DB
        $resetDb->reseted_at = date('Y-m-d H:i:s');
        $resetDb->save();

        // reset password to User
        $userDb->password = Hash::make($password);
        $userDb->save();

        DB::commit();

        $response->isSuccess = true;
        return $response;
    }

    /**
     * disable all active when request new reset password
     * @param $email
     */
    private static function disableAllActive($email){
        // get for the current email with still active
        self::where('email',$email)
            ->where('status','1')
            ->whereNull('reseted_at')
            ->where('valid_time','>=',date('Y-m-d'))
            ->update(['status'=>0]);
    }

}
