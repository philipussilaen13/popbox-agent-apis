<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    // set table
    protected $table = 'payment_methods';

    public static function getPaymentMethod($input){
        $transactionRef = $input['transaction_ref'];

        // get transaction DB
        $transactionDb = Transaction::where('reference',$transactionRef)->first();
        $module = $transactionDb->type;
        if (empty($module)) $module = 'purchase';

        // get available method
        $data = self::join('payment_available_module','payment_available_module.payment_methods_id','payment_methods.id')
            ->where('module',$module)->get();

        return $data;
    }
}
