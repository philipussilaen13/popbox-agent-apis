<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryOrderHistory extends Model
{
    protected $table = 'delivery_order_histories';

    /**
     * Insert New History for Delivery
     * @param $deliveryId
     * @param $status
     * @param null $remarks
     * @return \stdClass
     */
    public function newHistory($deliveryId,$status,$remarks=""){
        if (empty($remarks)) $remarks = "";
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $dataDb = new self();
        $dataDb->delivery_order_id = $deliveryId;
        $dataDb->status = $status;
        $dataDb->remarks = $remarks;
        $dataDb->save();

        $response->isSuccess = true;
        return $response;
    }
}
