<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentNonVA extends Model
{
    // table
    protected $table = 'payment_non_va';

    public static function createPayment($idPayment, $type, $data)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $paymentDb = new PaymentNonVA();
        $paymentDb->id_payment = $idPayment;
        $paymentDb->payment_id = $data->payment_id;
        $paymentDb->type = $type;
        $paymentDb->qr_code = $data->url;
        $paymentDb->transaction_id = $data->transaction_id;
        $paymentDb->save();

        if (!$paymentDb->save()) {
            return $response;
        }

        $response->isSuccess = true;
        return $response;
    }
}
