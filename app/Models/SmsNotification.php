<?php

namespace App\Models;

use App\Jobs\SendSMS;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SmsNotification extends Model
{
    // set tables
    protected $table = "sms_notifications";

    private $varModule = "";
    private $varVendor = "nexmo";
    private $varTo = "08563052300";
    private $varSms = "";
    private $varStatus = 0;

    /**
     * Create SMS Notification Queue
     * @param string $module
     * @param string $vendor
     * @param string $to
     * @param string $sms
     * @param int $status
     * @return \stdClass
     */
    public function createSMS($module = "",$vendor="",$to="08563052300",$sms = "", $status = 0){
        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // set variable
        $this->varModule = $module;
        $this->varVendor = $vendor;
        $this->varTo = $to;
        $this->varSms = $sms;
        $this->varStatus = 0;

        // insert to DB
        DB::beginTransaction();

        $notifDb = new self();
        $notifDb->module = $this->varModule;
        $notifDb->vendor = $this->varVendor;
        $notifDb->to = $this->varTo;
        $notifDb->sms = $this->varSms;
        $notifDb->status = $this->varStatus;

        $notifDb->save();

        // logging
        $this->log();

        DB::commit();

        $response->isSuccess = true;
        dispatch(new SendSMS($notifDb));
        return $response;
    }

    /**
     * logging
     */
    private function log(){
        $sms = json_encode($this->varSms);
        $msg = "'$this->varModule' $this->varVendor:$this->varTo '$sms'";
        $msg = "SMS $msg\n";
        $f = fopen(storage_path().'/logs/notification/'.date('Y.m.d.').'log','a');
        fwrite($f,date('H:i:s')." $msg");
        fclose($f);
        return;
    }
}
