<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransactionLocationReport extends Model
{
    protected $table = 'transaction_location_reports';
}
