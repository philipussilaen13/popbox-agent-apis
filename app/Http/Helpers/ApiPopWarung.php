<?php
/**
 * User: faisal
 * Date: 11/12/2018
 * Time: 14.34
 */

namespace App\Http\Helpers;


use Illuminate\Support\Facades\DB;

class ApiPopWarung
{
    private $url = null;
    private $token;

    public function __construct() {
    }

    /**
     * @param $request
     * @param array $param
     * @return mixed
     */
    public static function callAPI($method, $url, $data) {
        $curl = curl_init();

        switch ($method){
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        // OPTIONS:
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
        ));

//        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
//            'APIKEY: 111111111111111111111',
//            'Content-Type: application/json',
//        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
//        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        // EXECUTE:
        $result = curl_exec($curl);
        if(!$result){die("Connection Failure");}
        curl_close($curl);
        return $result;
    }
}