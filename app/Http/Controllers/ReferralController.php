<?php

namespace App\Http\Controllers;

use App\Models\Locker;
use App\Models\ReferralCampaign;
use App\User;
use Illuminate\Http\Request;

class ReferralController extends Controller
{
    /**
     * Check Referral
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkReferral(Request $request){
        $required = ['phone','email','referral_code'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // query user
        $phone = $request->input('phone');
        $email = $request->input('email');
        $referralCode = $request->input('referral_code',null);

        $userDb = User::where('phone',$phone)
            ->orWhere('email',$email)
            ->first();
        if ($userDb){
            $message = 'User Phone / Email Already Exist';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        if (empty($referralCode)){
            $message = 'Invalid Referral / Empty Code';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // check code from campaign first
        $referralCampaignDb = ReferralCampaign::where('code',$referralCode)->first();
        $isSpecialCampaign = true;
        if (!$referralCampaignDb){
            // if not available on referral campaign db check on lockers db
            $lockerDb = Locker::where('referral_code',$referralCode)->first();
            if (!$lockerDb){
                // referral code not found / invalid
                $message = 'Referral Code Not Found / Invalid.';
                $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                return response()->json($resp);
            }
            // get referral active campaign
            $check = ReferralCampaign::getActiveReferralCampaign();
            if (!$check->isSuccess){
                $message = $check->errorMsg;
                $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                return response()->json($resp);
            }
            $referralCampaignDb = ReferralCampaign::find($check->campaignId);
            $isSpecialCampaign = false;
        }

        // check usage
        $checkUsage = ReferralCampaign::checkUsage($referralCode);
        if (!$checkUsage->isSuccess){
            $message = $checkUsage->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $limitUsage = $checkUsage->limitUsage;
        $usage = $checkUsage->usage;

        if (!empty($limitUsage)){
            if ($usage >= $limitUsage){
                $message = 'Kode referral ini sudah mencapai maksimum pemakaian.';
                $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                return response()->json($resp);
            }
        }

        $dataResponse = new \stdClass();
        $dataResponse->name = $referralCampaignDb->name;
        $dataResponse->description = $referralCampaignDb->description;
        $dataResponse->expired = $referralCampaignDb->expired;
        $dataResponse->limit_usage = $limitUsage;
        $dataResponse->usage = $usage;

        $resp=['response' => ['code' => 200,'message' =>null], 'data' => [$dataResponse]];
        return response()->json($resp);
    }
}
