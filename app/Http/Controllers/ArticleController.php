<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function getArticles(Request $request){
        $required = [];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // get active
        $articlesDb = Article::listArticles($input);

        $articlesList = [];
        foreach ($articlesDb as $item){
            $tmp = new \stdClass();
            $tmp->promo_id = $item->id;
            $tmp->type = $item->type;
            $tmp->value = $item->value;
            $tmp->title_header = $item->title;
            $tmp->short_description = $item->short_description;
            $tmp->long_description = $item->long_description;
            if (empty($item->image_url)) $tmp->image_header = asset('articles')."/".$item->image;
            else $tmp->image_header = $item->image_url;
            $articlesList[] = $tmp;
        }
        if (empty($articlesList)){
            $message = "Empty News/Promo";
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $resp=['response' => ['code' => 200,'message' =>null], 'data' => $articlesList];
        return response()->json($resp);
    }
}
