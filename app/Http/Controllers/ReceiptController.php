<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use App\User;
use App\Models\Receipt;
use App\Models\ReceiptBatch;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Jobs\ProcessUpdGRBatch;

class ReceiptController extends Controller
{
    public function getReceiptByTransaction(Request $request){
        //required param list
        $required = ['transaction_ref','statusid','startdate','enddate'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
         //if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $username = $request->input('username');
        $transactionRef = $request->input('transaction_ref');
        $statusId = $request->input('statusid');
        $startDate = $request->input('startdate');
        $endDate = $request->input('enddate');

        $userDb = User::where('phone',$username)->first();
        if (!$userDb){
            $message = 'User tidak ditemukan';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $userID = $userDb->id;

        $receiptDb = Receipt::getReceipt($transactionRef, $userID, $statusId, $startDate, $endDate);

        if ( !$receiptDb->isSuccess){
            $message = $receiptDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $data = $receiptDb->data;
        $resp=['response' => ['code' => 200,'message' =>null], 'data' => $data];
        return response()->json($resp);
    }

    public function updateReceiptByBatch(Request $request) {
        //required param list
        $required = ['transaction_ref', 'batch_number', 'receipt_items'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
         //if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $response = ['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($response);
        }

        $username = $request->input('username');
        $transactionRef = $request->input('transaction_ref');
        $batchNumber = $request->input('batch_number');
        $receiptItems = $request->input('receipt_items');

        foreach ($receiptItems as $key => $r) {
            $field = array_keys($r);
            if( !in_array('item_id', $field)) $paramFailed[] = 'item_id';
            if( !in_array('qty_receipt', $field)) $paramFailed[] = 'qty_receipt';
            if( !in_array('gap_qty', $field)) $paramFailed[] = 'gap_qty';

        }

        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $response = ['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($response);
        }

        $userDb = User::where('phone',$username)->first();
        if (!$userDb){
            $message = 'User Not Found';
            $response = ['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($response);
        }
        $userId = $userDb->id;
        $receiptDb = Receipt::where('reference', $transactionRef)->first();

        $updReceiptDb = Receipt::updQtyReceiptByBatch($transactionRef, $username, $batchNumber, $receiptItems);
        if( $updReceiptDb->isSuccess) {
            dispatch(new ProcessUpdGRBatch($receiptDb, $receiptItems));
            $receiptDb = Receipt::leftJoin('receipt_batch', 'receipt.id', '=', 'receipt_batch.receipt_id')
                ->where('receipt.reference', $transactionRef)
                ->where('receipt_batch.batch_no', $batchNumber)
                ->select(
                    DB::raw("
                        receipt.id as receiptid, receipt.status_receipt as status_receipt_id,
                        case 
                            when receipt.status_receipt = 1 then 'Belum Dikonfirmasi'
                            when receipt.status_receipt = 2 then 'Terima Sebagian'
                            when receipt.status_receipt = 3 then 'Sudah Terima Semua Barang'
                            else null
                        end as status_receipt
                    ")
                );

            if($receiptDb->count() == 0) {
                $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                return response()->json($resp);    
            }

            $receiptDb = $receiptDb->first();
            $receiptBatchDb = ReceiptBatch::getReceiptBatchByNumber($receiptDb->receiptid, $batchNumber);
            $receiptBatchDb['status_receipt_id'] = $receiptDb->status_receipt_id;
            $receiptBatchDb['status_receipt'] = $receiptDb->status_receipt;
            $response = [
                'response' => [
                    'code' => 200,
                    'message' => 'Data has been success'
                ], 
                'data' => [$receiptBatchDb]
            ];
        }
        else {
            $response = [
                'response' => [
                    'code' => 400,
                    'message' => 'Data has been failure'
                ], 
                'data' => []
            ];
        }

        return response()->json($response);
    }
}
