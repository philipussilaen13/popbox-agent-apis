<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Transaction extends Mailable
{
    use Queueable, SerializesModels;
    private $transaction = '';

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($transaction)
    {
        $this->transaction = $transaction;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = [];
        $cc_receipt = ['sigit@popbox.asia','erwin@popbox.asia','raga@popbox.asia','ria@popexpress.id','bilkis@popbox.asia'];
        if(env('APP_ENV') == "development"){
            array_push($cc_receipt, 'officemail.faisal@gmail.com');
        }
        $data['transactions'] = $this->transaction[0];
        $data['transaction_items'] = $this->transaction;
        return $this->from('no-reply@popbox.asia')            
            ->cc($cc_receipt)
            ->subject('Pesanan Belanja Stok Dengan No Transaksi : '.$data['transactions']->reference)
            ->view('email.transaction.transaction', $data);
    }
}
