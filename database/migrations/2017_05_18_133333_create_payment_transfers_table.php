<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_transfers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('payments_id',false,false);
            $table->string('destination_bank',45);
            $table->string('destination_account',100);
            $table->string('sender_bank',45)->nullable()->default(NULL);
            $table->string('sender_name',100)->nullable()->default(NULL);
            $table->string('remarks',255)->nullable()->default(NULL);
            $table->string('picture',255)->nullable()->default(NULL);
            $table->timestamps();
            $table->dateTime('deleted_at')->nullable();

            $table->foreign('payments_id')->references('id')->on('payments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_transfers');
    }
}
