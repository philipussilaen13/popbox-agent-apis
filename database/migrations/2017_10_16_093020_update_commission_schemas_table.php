<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCommissionSchemasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('commission_schemas', function (Blueprint $table) {
            // create new column
            $table->integer('max_amount')->after('values')->nullable();
            $table->integer('min_amount')->after('max_amount')->nullable();
            $table->integer('priority')->after('status')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('commission_schemas', function (Blueprint $table) {
            $table->dropColumn('max_amount');
            $table->dropColumn('min_amount');
            $table->dropColumn('priority');
        });
    }
}
