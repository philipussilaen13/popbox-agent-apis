<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationFcmTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification_fcm', function (Blueprint $table) {
            $table->increments('id');
            $table->string('module',128);
            $table->string('to',255);
            $table->string('device',64);
            $table->string ('title',128);
            $table->string('body',255);
            $table->text('data')->nullable();
            $table->string('status',64);
            $table->text('parameter')->nullable();
            $table->text('response')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notification_fcm');
    }
}
