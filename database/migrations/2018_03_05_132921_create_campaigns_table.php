<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaigns', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',256)->comment('Name of Campaign');
            $table->text('description')->nullable();
            $table->dateTime('start_time')->comment('Starting time of Campaign');
            $table->dateTime('end_time')->comment('Ending time of Campaign');
            $table->string('status',64)->default('disabled')->comment('Status of Campaign: enabled, disabled');
            $table->string('promo_type',64)->default('purchase')->comment('Promo Type: purchase, topup. If Purchase, then deduct the amount must pay. If Topup, then add more money to user get');
            $table->string('type',64)->default('percent')->comment('Type Of Promo. Fixed: amount fix promo 10.000. Percent, Amount Promo based on xx%(10%) based on price');
            $table->string('category',64)->default('fixed')->comment('Type of Promo. Fixed: total amount transaction will be X Amount. Percent: Total Transaction will be deducted X Amount');
            $table->smallInteger('voucher_required')->default(1);
            $table->string('campaign_limit_type')->default('all')->comment('Limit Voucher Type. All: global usage limit. Member: per member based limit. Voucher: voucher based in limit');
            $table->integer('limit_usage',false,false)->nullable();
            $table->integer('amount',false,false);
            $table->integer('max_amount',false, false)->nullable();
            $table->integer('min_amount',false,false)->nullable();
            $table->integer('priority',false,false)->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaigns');
    }
}
