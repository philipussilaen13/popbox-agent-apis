<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryDistricts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_districts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('delivery_city_id',false,true);
            $table->string('code',20);
            $table->string('district_name',255);
            $table->timestamps();

            $table->foreign('delivery_city_id')->references('id')->on('delivery_cities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_districts');
    }
}
