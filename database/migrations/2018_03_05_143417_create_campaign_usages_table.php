<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignUsagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign_usages', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('campaign_id');
            $table->unsignedInteger('voucher_id')->nullable();
            $table->integer('user_id',false,false);
            $table->integer('transaction_id',false,false);
            $table->integer('transaction_amount',false,false);
            $table->integer('discount_amount',false,false);
            $table->integer('final_amount',false,false);
            $table->smallInteger('status_usage',false,false);
            $table->dateTime('time_limit')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('campaign_id')->references('id')->on('campaigns');
            $table->foreign('voucher_id')->references('id')->on('campaign_vouchers');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('transaction_id')->references('id')->on('transactions');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaign_usages');
    }
}
