<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionLocationReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_location_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->string('locker_id',100);
            $table->integer('transaction_id',false,false);
            $table->integer('transaction_item_id',false,false);
            $table->string('reference',128)->nullable();
            $table->string('category',64);
            $table->string('type',64);
            $table->string('brand',128)->nullable();
            $table->string('name',256);
            $table->string('product_id',128)->nullable();
            $table->integer('quantity');
            $table->integer('price');
            $table->dateTime('transaction_datetime');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_location_reports');
    }
}
