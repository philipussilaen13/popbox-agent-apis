<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableLockerVaOpen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locker_va_open', function (Blueprint $table) {
            $table->increments('id');
            $table->string('lockers_id',100)->nullable();
            $table->string('va_type',100)->nullable();
            $table->string('va_number',100)->nullable();
            $table->string('payment_id',100)->nullable();
            $table->dateTime('expired_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locker_va_open');
    }
}
