<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvailableParametersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('available_parameters', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',150);
            $table->string('param_name',150);
            $table->string('description',255)->nullable();
            $table->smallInteger('is_commission')->default(0);
            $table->smallInteger('is_campaign')->default(0);
            $table->text('value')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('available_parameters');
    }
}
