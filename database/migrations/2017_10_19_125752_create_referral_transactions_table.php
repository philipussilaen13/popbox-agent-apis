<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferralTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('referral_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('referral_campaign_id',false,true);
            $table->string('code',50);
            $table->string('from_locker_id',100)->nullable();
            $table->string('to_locker_id',100);
            $table->string('type',50);
            $table->integer('from_amount')->default(0);
            $table->integer('to_amount');
            $table->dateTime('submit_date');
            $table->dateTime('used_date');
            $table->dateTime('expired_date');
            $table->timestamps();

            $table->foreign('referral_campaign_id')->references('id')->on('referral_campaigns');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('referral_transactions');
    }
}
